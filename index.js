const express = require('express');
const connectDB = require('./database/db');
const PersonModel = require('./models/person');

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

connectDB();

app.get("/", (request, response) => {
    response.send("Hello from an Express API!");
});

// Define a route for creating and saving a new person
app.post('/create-person', async (req, res) => {
    try {
        const newPerson = new PersonModel({
            name: 'Yann',
            age: 19,
            favoriteFoods: ['Alloco', 'Poulet'], // Corrected the array syntax
        });
        const savedPerson = await newPerson.save();
        // (error, data) => {
        //     if (error) {
        //         console.error(error);
        //     } else {
        //         console.log('Enregistrement sauvegardé avec succès :', data);
        //     }
        // }
        res.send(savedPerson);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route for creating and saving many new persons
app.post('/create-many-persons', async (req, res) => {
    try {
        const newPersons = await PersonModel.create([
            {
                name: 'Mary',
                age: 22,
                favoriteFoods: ['Riz', 'Poulet', 'Foutou'],
            },
            {
                name: 'Code',
                age: 17,
                favoriteFoods: ['Foufou', 'Agouti', 'Cabri'],
            },
            {
                name: 'Mary',
                age: 31,
                favoriteFoods: ['Wantche', 'Bawin'],
            },
            {
                name: 'Nyash',
                age: 65,
                favoriteFoods: ['Alloco', 'Placali'],
            },
            {
                name: 'Mary',
                age: 34,
                favoriteFoods: ['Alloco', 'Poulet', 'Donkounou'],
            }
        ]);

        console.log('Enregistrements sauvegardés avec succès :', newPersons);
        res.send(newPersons);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to find people by name
app.get('/find-by-name/:name', async (req, res) => {
    const { name } = req.params;
    try {
        const peopleWithGivenName = await PersonModel.find({ name });
        console.log(`People with name '${name}':`, peopleWithGivenName);
        res.send(peopleWithGivenName);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to find people by favorite food
app.get('/findOne-by-food/:food', async (req, res) => {
    const { food } = req.params;
    try {
        const peopleWithGivenFood = await PersonModel.findOne({ favoriteFoods: food });
        console.log(`People with food '${food}':`, peopleWithGivenFood);
        res.send(peopleWithGivenFood);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to find people by id
app.get('/find-by-id/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const peopleWithGivenId = await PersonModel.findById(id);
        if (!peopleWithGivenId) {
            console.log(`People with id '${id}' was not find:`);
            return res.status(404).send('Person not found')
            return
        }
        console.log(`People with id '${id}':`, peopleWithGivenId);
        res.send(peopleWithGivenId);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to update people favourite foods with hamberger by given id
app.put('/update-food-by-id/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const personToUpdate = await PersonModel.findById(id);

        if (!personToUpdate) {
            return res.status(404).send('Person not found');
        }
        personToUpdate.favoriteFoods.push('hamburger');
        const updatedPerson = await personToUpdate.save();
        console.log(`Person with id '${id}' updated successfully:`, updatedPerson);
        res.send(updatedPerson);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to update people age to 20 
app.put('/update-age-by-name/:name', async (req, res) => {
    const { name } = req.params;
    try {
        const updatedPerson = await PersonModel.findOneAndUpdate(
            { name },
            { age: 20 },
            { new: true } // This ensures that the updated document is returned
        );
        if (!updatedPerson) {
            return res.status(404).send('Person not found');
        }
        console.log(`Person with name '${name}' updated successfully:`, updatedPerson);
        res.send(updatedPerson);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to delete people by id
app.delete('/delete-by-id/:id', async (req, res) => {
    const { id } = req.params
    try {
        const deletedPerson = await PersonModel.findOneAndDelete({ _id: id })
        if (!deletedPerson) {
            return res.status(404).send('Person not found');
        }
        res.send({ message: `Person with id '${id}' deleted successfully` })
    } catch (error) {
        res.status(500).send(error.message);
    }
})

// Define a route to delete all peoples with name Mary
app.delete('/delete-all-mary', async (req, res) => {
    try {
        const result = await PersonModel.deleteMany({ name: "Mary" });
        if (result.deletedCount === 0) {
            return res.status(404).send('No persons with name "Mary" found');
        }
        res.send({ message: `Persons with name "Mary" were deleted successfully` });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Define a route to find people who like burritos
app.get('/find-burritos-lovers', async (req, res) => {
    try {
        const burritosLovers = await PersonModel.find({ favoriteFoods: "Burritos" }).sort().limit(2).select('-age').exec()
        console.log(burritosLovers);
        res.send(burritosLovers)
    } catch (error) {
        res.status(500).send(error.message);
    }
})

app.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}`);
});
